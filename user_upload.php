<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/8/2019
 * Time: 6:58 AM
 * ====================================== REQUIREMENTS ==========================================
 * 1. Script Task
 * Create a PHP script, that is executed from the command line, which accepts a CSV file as input
 * (see command line directives below) and processes the CSV file. The parsed file data is to be
 * inserted into a MySQL database. A CSV file is provided as part of this task that contains test
 * data, the script must be able to process this file appropriately.
 * The PHP script will need to correctly handle the following criteria:
 * • CSV file will contain user data and have three columns: name, surname, email
 * (see table definition below)
 * • CSV file will have an arbitrary list of users
 * • Script will iterate through the CSV rows and insert each record into a dedicated MySQL
 * database into the table “users”
 * • The users database table will need to be created/rebuilt as part of the PHP script.
 * This will be defined as a Command Line directive below
 * • Name and surname field should be set to be capitalised e.g. from “john” to “John” before
 * being inserted into DB
 * • Emails need to be set to be lower case before being inserted into DB
 * • The script should validate the email address before inserting, to make sure that it
 * is valid (valid means that it is a legal email format, e.g. “xxxx@asdf@asdf” is not
 * a legal format). In case that an email is invalid, no insert should be made to
 * database and an error message should be reported to STDOUT.
 * We are looking for a script that is robust and gracefully handles errors/exceptions.
 * The PHP script command line argument definition is outlined in 1.4 Script Command Line
 * Directives . However, user documentation will be looked upon favourably.
 */

error_reporting(error_reporting() & ~E_NOTICE);

// Get all options given in command line
$options = getopt("u:p:h:c:t:d:");

// Get all arguments
$arguments = null;
foreach ($argv as $k => $v) {
    $temp = explode('=', $v);
    $arguments[$temp[0]] = $temp[1];
}

// Set directives for --help command line argument
$directives = "The PHP script should include these command line options (directives):
 --file [csv file name] – this is the name of the CSV to be parsed
 --create_table – this will cause the MySQL users table to be built (and no further action will be taken)
 --dry_run – this will be used with the --file directive in case we want to run the script but not insert into the DB. All other functions will be executed, but the database won't be altered
 -u – MySQL username
 -p – MySQL password
 -h – MySQL host
 --help – which will output the above list of directives with details.
 ";

// Display help directives and do nothing else
if (in_array("--help", $argv)) {
    die($directives);
}

// Filename and path from the dir that has this script
if (in_array("--file", array_keys($arguments))) {
    $filePath = $arguments['--file'];
}
// Set not to insert into database
$dryRun = false;
if (in_array("--dry_run", $argv) && !isset($arguments["--file"])) {
    die('You must specify the csv file location with directive --file. For instance: --file=PATH_TO_CSV_FILE');
} elseif (in_array("--dry_run", $argv) && isset($arguments["--file"])) {
    $dryRun = true;
}

// Db connection credentials
$dbHost = isset($options['h']) ? $options['h'] : '127.0.0.1';
$dbName = isset($options['d']) ? $options['d'] : 'user_upload';
$dbUsername = isset($options['u']) ? $options['u'] : 'root';
$dbPassword = isset($options['p']) ? $options['p'] : '';

// Set column options. Default is name, surname and email with indexes in that order
$columns = isset($options['c']) ? explode(',', $options['c']) : ['name', 'surname', 'email'];

// If options "t" is passed set the table name as "t" value
$table = isset($options['t']) ? $options['t'] : 'users';

// Create table
if (in_array("--create_table", $argv)) {

    // Create table here:
    try {
        $sql = "DROP TABLE IF EXISTS $table;";
        $pdo = getDatabaseConnection($dbHost, $dbName, $dbUsername, $dbPassword);
        $pdo->exec($sql);

        $sql = "CREATE TABLE $table ( name VARCHAR( 100 ) NOT NULL,  surname VARCHAR( 200 ) NOT NULL,  email VARCHAR( 150 ) NOT NULL UNIQUE);";
        $pdo = getDatabaseConnection($dbHost, $dbName, $dbUsername, $dbPassword);
        $pdo->exec($sql);

    } catch (PDOException $exception) {
        die("PDO Error: " . $pdo->errorInfo()[2]);
    }

    die("Created/Rebuilt $table Table.\n");
}

try {
    // Opening file given by location
    $file = fopen($filePath, 'r'); // The CSV file we are reading

    // Count array - used to collect stats
    $counts = array(
        'total' => 0,
        'duplicate' => 0,    // The email was present in the data supplied more than once (note each duplicate of the same address adds to the count)
        'invalid' => 0,         // The email did not validate so record not added
        'exists' => 0,       // The email already existed but was not updated because nothing had changed in the fields provided
        'new' => 0           // New contact added
    );


} catch (\Exception $exception) {
    die($exception->getMessage());
}

// Getting pdo connection
$pdo = getDatabaseConnection($dbHost, $dbName, $dbUsername, $dbPassword);

// Begin transaction
$pdo->beginTransaction();

// Prepare queries
$queryFindRowByEmails = $pdo->prepare("SELECT * FROM $table WHERE email = :email");

$insertRowQuery = 'INSERT INTO ' . $table . ' (' . implode(', ', array_values($columns)) . ') VALUES (:' . implode(', :', array_values($columns)) . ')';

$queryInsertPreparation = $pdo->prepare($insertRowQuery);

// Processed emails
$processed = array();

echo 'Starting loop' . "\r\n";

// If this is set to true it means csv file has headers and first row will be skipped
$skipNextRow = true;

// Do all inserts in a single transaction
try {
    // Step through rows to insert
    while ($row = fgetcsv($file)) {

        if ($skipNextRow) {
            $skipNextRow = false;
            continue;
        }

        $counts['total']++;
        echo $counts['total'] . "\r";

        // Create the data row
        $data = [];

        foreach (array_values($columns) as $index => $value) {
            $data[$value] = strtolower($value) !== 'email' ? ucfirst(strtolower($row[$index])) : strtolower($row[$index]);
        }

        // Check for duplicate
        if (in_array($data['email'], $processed)) {
            $counts['duplicate']++;
            continue;
        }

        $processed[] = $data['email'];
        // Check for valid email address
        if (!$data['email'] || !validateEmailAddress($data['email'])) {
            // Invalid email skip
            $counts['invalid']++;
            echo $data['email'] . 'is an invalid email address' . "\n";
            continue;
        }

        // Check for existing email
        $queryFindRowByEmails->execute(array('email' => $data['email']));
        if ($r = $queryFindRowByEmails->fetch(PDO::FETCH_ASSOC)) {
            $counts['exists']++;

        } else {
            // The email doesn't exist
            $fields = array('name' => $data['name'], 'surname' => $data['surname'], 'email' => $data['email']);
            if (!$dryRun) {
                $queryInsertPreparation->execute($fields);
            }
            $counts['new']++;
        }

    }
    $pdo->commit();

} catch (Exception $exception) {
    // Rollback on exception
    $pdo->rollBack();
    throw $exception;
}

fclose($file);
print_r($counts);
if(!$dryRun){
    echo 'CSV Import run completed' . "\n";
}else{
    echo 'CSV Import DRY run completed' . "\n";
}


/**
 * This method will verify if the email given per row is valid as per requirements
 * @param $emailAddress
 * @return bool|null
 */
function validateEmailAddress($emailAddress)
{
    return filter_var($emailAddress, FILTER_VALIDATE_EMAIL) ? true : null;
}

/**
 * This method will retrieve the database connection
 * @param $dbHost
 * @param $dbName
 * @param $dbUsername
 * @param $dbPassword
 * @return PDO
 */
function getDatabaseConnection($dbHost, $dbName, $dbUsername, $dbPassword)
{
// Get database connection
    try {
        $pdo = new PDO("mysql:dbHost=$dbHost;dbname=$dbName", $dbUsername, $dbPassword);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $exception) {
        die ($exception->getMessage());
    }
    return $pdo;
}