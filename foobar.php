<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/8/2019
 * Time: 6:31 AM
 * ============================== REQUIREMENTS ===================================
 * 2. Logic Test
 * Create a PHP script that is executed form the command line. The script should:
 * • Output the numbers from 1 to 100
 * • Where the number is divisible by three (3) output the word “foo”
 * • Where the number is divisible by five (5) output the word “bar”
 * • Where the number is divisible by three (3) and (5) output the word “foobar”
 * • Only be a single PHP file
 * 2.1 Example
 * An example output of the script would look like:
 * 1, 2, foo, 4, bar, foo, 7, 8, foo, bar, 11, foo, 13, 14, foobar …
 * 2.2 Deliverable
 * The deliverable for this task is a PHP script called foobar.php.
 * There is no need to put this task into source control (but you can if you want to).
 */

error_reporting(error_reporting() & ~E_NOTICE);

$count = 0;
$max = 100;
$lineBreakChar = "";
if (in_array('--break_line', $argv)) {
    $lineBreakChar = "\n";
}
while ($count < 100) {
    $count++;
    if ($count % 3 == 0 & $count % 5 == 0) {
        echo 'FooBar' . $lineBreakChar;
    } else if ($count % 3 == 0) {
        echo 'Foo' . $lineBreakChar;
    } else if ($count % 5 == 0) {
        echo 'Bar' . $lineBreakChar;
    } else {
        echo $count . $lineBreakChar;
    }
}